// MUSHROOM BOX
// calafou.org

#define DHT11_PIN 8        // señal del sensor DHT11 conectada al pin D8
#define RELAY1 7             //  señal del relay al pin D7
int photocellPin = 0;          // señal  fotosensor conectada al pin A0
int photocellReading;       //  lectura del sensor de luz

void setup(){
  rtc.begin();
  Serial.begin(9600);
  pinMode(RELAY1, OUTPUT); 
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));     // Ponemos en hora, solo la primera vez, luego comentar y volver a cargar el sketch en la arduino.
}

void loop()
{
    
  DateTime now = rtc.now();
  int chk = DHT.read11(DHT11_PIN);
  //int sensorValue = analogRead(A1);
  photocellReading = analogRead(photocellPin);  

  if (DHT.temperature >=20 || DHT.humidity >=70){
    digitalWrite(RELAY1,LOW);
  }
  else{
    digitalWrite(RELAY1,HIGH);
  }
  

  Serial.print(now.year());
  Serial.print('/');
  Serial.print(now.month());
  Serial.print('/');
  Serial.print(now.day());
  Serial.print(" ");
  Serial.print(now.hour());
  Serial.print(':');
  Serial.print(now.minute());
  Serial.print(':');
  Serial.print(now.second());
  Serial.println();
  Serial.print("Temperatura ambiente = ");
  Serial.println(DHT.temperature);
  Serial.print("Humedad ambiente = ");
  Serial.println(DHT.humidity);
  Serial.print("Luz ambiente = ");
  Serial.println(photocellReading);
  Serial.println("");
  delay(5000);
}